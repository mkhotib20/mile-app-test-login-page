import React, { Component } from 'react'
import {Button, Col, FormGroup, Row, Spinner} from 'reactstrap'
import './App.scss'
import PhoneIcon from './PhoneIcon'
import Swicth from './Swicth'

const language = {
	back:{
		ID: "Kembali ke Mile",
		EN: "Back to Mile"
	},
	cn: {
		ID: "Nama Perusahaan",
		EN: "Company Name"
	},
	yw: {
		ID: "Platform Anda untuk mengelola semua manajemen layanan di lapangan",
		EN: "Your one stop platform to manage all of your field service management"
	},
	nry: {
		ID: "Belum terdaftar?",
		EN: "Not registered yet?"
	},
	cu: {
		ID: "Hubungi kami",
		EN: "Contact us"
	},
	fmi: {
		ID: "untuk info lebih lanjut",
		EN: "for more info"
	}
}

class App extends Component<{},{
	lan: "ID" | "EN"
	submitting: boolean
	errors: {
		attr: string,
		msg: string
	} | null
}> {
	private inputRef: React.RefObject<any>
	constructor(props: any) {
		super(props)
		this.inputRef = React.createRef()
		this.state = {
			submitting: false,
			lan: "EN",
			errors: null
		}
	}
	submit = (e: any) => {
		e.preventDefault()
		e.stopPropagation()
		let fd = new FormData(e.target)
		let compName = fd.get("company_name")
		if (compName=="") {
			this.setState({errors: {
				attr: "company_name",
				msg: "Compnay name cannot be empty"
			}},()=>{
				this.inputRef.current.focus()
			})
			return
		}
		this.setState({submitting: true}, ()=>{
			setTimeout(() => {
				this.setState({
					errors: {
						attr: "company_name",
						msg: `Opps, ${compName}.mile.app is not available `
					},submitting: false
				},()=>{
					this.inputRef.current.focus()
				})
			}, 1200);
		})
	}
	render() {
		return (
			<div className="login-page">
				<Row>
					<Col md="6">
						<div className="login-box">
							<div className="login-content">
								<div className="text-center col-md-6 offset-md-3">
									<img className="img-fluid w-100 mb-3" src="https://taskdev.mile.app/69ece7b1819e760b63623e810922b59e.png" alt="Mile icon"/>
									<p className="text-subtitle">{language.yw[this.state.lan]}</p>
								</div>
								<div className="col-md-8 offset-md-2 mt-2 mt-md-5">
									<form onSubmit={this.submit}>
										<div className={`form__group ${this.state.errors!=null ? "error" : ""}`}>
											<input ref={this.inputRef} disabled={this.state.submitting} onChange={()=>{
												this.setState({errors: null})
											}} name="company_name" autoFocus type="text" className="form__field" placeholder="company name" />
											<label className="form__label">{language.cn[this.state.lan]}</label>
										</div>
										{this.state.errors!=null ? (
											<small style={{color: "red"}}>{this.state.errors.msg}</small>
										) : false}
										<FormGroup className="mt-5">
											<Button disabled={this.state.submitting} block color="primary">
												{this.state.submitting ? (
													<>
													<Spinner size="sm" color="light" /> Logging in
													</>
												) : "LOGIN"}
											</Button>
										</FormGroup>
									</form>
									<div className="text-center mt-4">
											<p className="text-muted">{language.nry[this.state.lan]} <a href="https://taskdev.mile.app/request-demo">{language.cu[this.state.lan]}</a> {language.fmi[this.state.lan]}</p>
									</div>
								</div>
							</div>
						</div>
						<div className="text-center footer">
							<p>© Copyright 2019 PT. Paket Informasi Digital. All Rights Reserved</p>
						</div>
					</Col>
					<div className="topbar">
					<a href="https://dev.mile.app/" className="text-prumary">&#129044; {language.back[this.state.lan]} </a>

						<div className="float-right">
							<Swicth change={()=>{
								this.setState({
									lan: this.state.lan=="EN" ? "ID" : "EN"
								})
							}} lan={this.state.lan} />
							{/* <Button color="light" size="sm">ID</Button> */}
						</div>
						<div className="mx-4 float-right">
							<a href="tel: +62 812-1133-5608" className="btn btn-rouded btn-sm btn-white text-primary px-3" ><PhoneIcon/> +62 812-1133-5608</a>
						</div>
					</div>
					<div className="login-bg" style={{
						background: 'url("https://taskdev.mile.app/d09f7f2de7d88fb5f1575273b8a26426.png") center center no-repeat',
						backgroundSize: "cover",
					}}></div>
				</Row>
			</div>	
		)
	}
}

export default App
