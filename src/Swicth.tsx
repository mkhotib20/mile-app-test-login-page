import React, { Component } from 'react'

type SwicthProps = {
    lan: "ID" | "EN"
    change: any
}

const Swicth = (props: SwicthProps) => {
    
    return (
        <div className="switch">
            <div onClick={props.change} className={`box ${props.lan=="EN"  ? "active" : ""}`}>
                <div className="child"></div>
                <div className="text">{props.lan}</div>
            </div>
        </div>
    )
}

export default Swicth
